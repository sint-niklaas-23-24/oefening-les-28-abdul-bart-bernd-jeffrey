﻿namespace Oefening_28._7___Digitale_Kluis
{
    class DigitaleKluis
    {
        private int _code;
        private bool _canshowcode = false;
        private int _codelevel;
        private int _aantalPogingen = 0;

        public DigitaleKluis(int code)
        {
            _code = code;
        }
        public int Code
        {
            get 
            {
                if (CanShowCode)
                {
                    return _code;
                }
                else
                { return -666; }
               
            }
           private set
            { 
                { _code = value; }               
            }
        }
        public bool CanShowCode
        {
            get { return _canshowcode; }
            set { _canshowcode = value; }
        }
        public int CodeLevel
        {
            get { return (_code /1000); }
        }
        public int AantalPogingen
        {
            get { return _aantalPogingen; }
            set { _aantalPogingen = value; }
        }
        public bool TryCode(int guess)
        {

            if (_code == guess)
            {
                AantalPogingen++;
                CanShowCode = true;
                return true;
            }
            else
            {
                AantalPogingen++;
                return false;
            }
        }
        public static void BruteForceren(DigitaleKluis slachtoffer)
        {
            int guessCode = 100000;
            while (slachtoffer.CanShowCode == false) 
            {
                slachtoffer.TryCode(guessCode);
                guessCode++;
            }
        }
    }
}
