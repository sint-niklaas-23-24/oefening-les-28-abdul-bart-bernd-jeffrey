﻿using System.Media;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Oefening_28._7___Digitale_Kluis
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Random mijnCode = new Random();
        DigitaleKluis mijnKluis;


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            mijnKluis = new DigitaleKluis(mijnCode.Next(100000, 999999));
        }

        private void btnInvoeren_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (txtInvoer.Text == "-666")
                {

                    throw new Exception("Valsspelen mag niet. Je zou toch ook niet een auto downloaden.");
                }
                 if (!string.IsNullOrWhiteSpace(txtInvoer.Text))
                {
                    if (txtInvoer.Text.All(char.IsDigit))
                    {
                        if (!mijnKluis.TryCode(Convert.ToInt32(txtInvoer.Text)))
                        {
                            lblUitvoer.Content = "Verkeerde code. Code: " + mijnKluis.Code + Environment.NewLine + "U zit al aan poging " + mijnKluis.AantalPogingen;
                        }
                        else
                        {
                            imgKluis.Source = new BitmapImage(new Uri("Images/kluis_open.jpeg", UriKind.Relative));
                            lblUitvoer.Content = "Juiste code. Code: " + mijnKluis.Code + Environment.NewLine + "U had " + mijnKluis.AantalPogingen + " pogingen nodig.";
                        }
                    }
                    else
                    {
                        MessageBox.Show("Gelieve enkel cijfers in te vullen.", "Oops", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Gelieve geen lege input te geven.", "Oops", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex) 
            {
                MediaPlayer mediaPlayer = new MediaPlayer();
                mediaPlayer.Open(new Uri(@"C:\Users\bernd\Desktop\Visual Studio\Oefeningen\Oefening 28\oefening-les-28-abdul-bart-bernd-jeffrey\sounds\49_20siren.mp3", UriKind.Absolute));
                mediaPlayer.Play();
                //SoundPlayer soundPlayer = new SoundPlayer(@"C:\Users\bernd\Desktop\Visual Studio\Oefeningen\Oefening 28\oefening-les-28-abdul-bart-bernd-jeffrey\sounds\49_20siren.mp3");
                //soundPlayer.Play();
                MessageBox.Show(ex.Message, "Piratery is een misdrijf", MessageBoxButton.OK,MessageBoxImage.Error);
            }
        }

        private void btnKraakCode_Click(object sender, RoutedEventArgs e)
        {

            DigitaleKluis.BruteForceren(mijnKluis);
            if (mijnKluis.CanShowCode == true)
            {
                imgKluis.Source = new BitmapImage(new Uri("Images/kluis_open.jpeg", UriKind.Relative));
                lblUitvoer.Content = "Juiste code. Code: " + mijnKluis.Code + Environment.NewLine + "Ik had " + mijnKluis.AantalPogingen + " pogingen nodig.";
            }
        }
    }
}